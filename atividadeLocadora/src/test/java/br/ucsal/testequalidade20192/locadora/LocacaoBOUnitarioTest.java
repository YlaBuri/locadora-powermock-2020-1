package br.ucsal.testequalidade20192.locadora;

import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyNoMoreInteractions;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.testequalidade20192.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20192.locadora.dominio.Cliente;
import br.ucsal.testequalidade20192.locadora.dominio.Locacao;
import br.ucsal.testequalidade20192.locadora.dominio.Modelo;
import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20192.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20192.locadora.persistence.VeiculoDAO;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LocacaoBO.class, ClienteDAO.class,VeiculoDAO.class, LocacaoDAO.class})
public class LocacaoBOUnitarioTest {

	/**
	 * Verificar se ao locar um veiculo disponivel para um cliente cadastrado, um
	 * contrato de loca�ao � inserido.
	 * 
	 * Metodo:
	 * 
	 * public static Integer locarVeiculos(String cpfCliente, List<String> placas,
	 * Date dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado
	 *
	 * Observa�ao1: lembre-se de mocar os metodos necessarios nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * Observa�ao2: lembre-se de que o metodo locarVeiculos e um metodo command.
	 * 
	 * @throws Exception
	 */
	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {

		SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yy");
		Cliente cliente = new Cliente("000.000.000-00", "Cliente", "3333-0000");
		Modelo modelo = new Modelo("Gol");
		Veiculo veiculo = new Veiculo("AAA-0000", 2010, modelo, 49.99);
		String placa = "AAA-0000";
		Date data = sdf.parse("20/05/2020");
		
		List<Veiculo> veiculos = new ArrayList<>();
		List<String> placas = new ArrayList<>();
		veiculos.add(veiculo);
		placas.add(placa);
		
		Locacao locacao = new Locacao(cliente, veiculos, data, 5);
		 
		mockStatic(ClienteDAO.class);
		when(ClienteDAO.obterPorCpf("000.000.000-00")).thenReturn(cliente);		
		mockStatic(VeiculoDAO.class);
		when(VeiculoDAO.obterPorPlaca("AAA-0000")).thenReturn(veiculo);		
		mockStatic(LocacaoDAO.class);
		whenNew(Locacao.class).withAnyArguments().thenReturn(locacao);
		LocacaoBO.locarVeiculos("000.000.000-00", placas, data, 5);

		verifyStatic(ClienteDAO.class);
		ClienteDAO.obterPorCpf("000.000.000-00");
		verifyStatic(VeiculoDAO.class);
		VeiculoDAO.obterPorPlaca("AAA-0000");
		verifyStatic(LocacaoDAO.class);
		LocacaoDAO.insert(locacao);
		verifyNoMoreInteractions(LocacaoBO.class);
	}
}
